# Maju Bois Theme

Maju Bois designs and manufactures custom furniture – http://www.majubois.be/

This repository is made to work on the website and structure of it built with grav cms. You can see the identity work and researches on the following repo → https://gitlab.constantvzw.org/osp/work.maju


The **Maju Bois** Theme is for [Grav CMS](http://github.com/getgrav/grav).  This README.md file should be modified to describe the features, installation, configuration, and general usage of this theme.


